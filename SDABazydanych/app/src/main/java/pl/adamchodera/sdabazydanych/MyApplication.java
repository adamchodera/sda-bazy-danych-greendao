package pl.adamchodera.sdabazydanych;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import pl.adamchodera.sdabazydanych.data.DaoMaster;
import pl.adamchodera.sdabazydanych.data.DaoSession;

public class MyApplication extends Application {

    private DaoSession daoSession;

    @Override
    public void onCreate() {
        super.onCreate();

        // do this once, for example in your Application class
        final DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(this, "Tasks.db");
        final SQLiteDatabase db = devOpenHelper.getWritableDatabase();
        final DaoMaster daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }
}

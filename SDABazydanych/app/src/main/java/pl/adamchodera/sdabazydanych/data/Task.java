package pl.adamchodera.sdabazydanych.data;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

@Entity(nameInDb = "tasks")
public class Task {

    public static final Long DEFAULT_ID_FOR_TASK_NOT_SAVED_IN_DB = -1L;

    @Id(autoincrement = true)
    private Long id;

    private String title;

    private String description;

    @Property(nameInDb = "completed")
    private boolean isCompleted;

    private long userId;

    @Generated(hash = 608119843)
    public Task(Long id, String title, String description, boolean isCompleted,
            long userId) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.isCompleted = isCompleted;
        this.userId = userId;
    }

    @Generated(hash = 733837707)
    public Task() {
    }

    public boolean isTaskInDatabase() {
        // task which wasn't inserted into database has null value
        if (id == null) {
            return false;
        } else {
            return true;
        }
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getIsCompleted() {
        return this.isCompleted;
    }

    public void setIsCompleted(boolean isCompleted) {
        this.isCompleted = isCompleted;
    }

    public long getUserId() {
        return this.userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
}

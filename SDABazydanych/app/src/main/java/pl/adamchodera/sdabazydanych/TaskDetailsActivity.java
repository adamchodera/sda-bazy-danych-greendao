package pl.adamchodera.sdabazydanych;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.adamchodera.sdabazydanych.data.DaoSession;
import pl.adamchodera.sdabazydanych.data.Task;
import pl.adamchodera.sdabazydanych.data.TaskDao;
import pl.adamchodera.sdabazydanych.data.User;
import pl.adamchodera.sdabazydanych.data.UserDao;

public class TaskDetailsActivity extends AppCompatActivity {

    public static final String INTENT_EXTRA_TASK_ID = "ARG_task_id";

    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.title_edit_text)
    EditText titleEditText;

    @BindView(R.id.description_edit_text)
    EditText descriptionEditText;

    @BindView(R.id.floating_action_button)
    FloatingActionButton floatingActionButton;

    @BindView(R.id.users_spinner)
    Spinner usersSpinner;

    private SaveOrEditTaskInDatabaseAsyncTask saveOrEditTaskInDatabaseAsyncTask;
    private boolean editMode = true;
    private Task currentTask;
    private TaskDao taskDao;
    private List<User> usersList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_details);
        ButterKnife.bind(this);

        DaoSession daoSession = ((MyApplication) getApplication()).getDaoSession();
        taskDao = daoSession.getTaskDao();
        final UserDao userDao = daoSession.getUserDao();

        initUsersSpinner(userDao);

        final long taskId = getIntent().getLongExtra(INTENT_EXTRA_TASK_ID, Task.DEFAULT_ID_FOR_TASK_NOT_SAVED_IN_DB);
        if (taskId != Task.DEFAULT_ID_FOR_TASK_NOT_SAVED_IN_DB) {
            // user clicked task from the list
            setReadonlyMode();

            // get the note DAO
            final QueryBuilder<Task> taskQueryBuilder = taskDao.queryBuilder();
            taskQueryBuilder
                    .where(TaskDao.Properties.Id.eq(taskId));

            final List<Task> list = taskQueryBuilder.list();
            currentTask = list.iterator().next();

            titleEditText.setText(currentTask.getTitle());
            descriptionEditText.setText(currentTask.getDescription());

            final long userId = currentTask.getUserId();
            final int spinnerPositionForUser = getSpinnerPositionForUser(userId);
            usersSpinner.setSelection(spinnerPositionForUser);


        } else {
            currentTask = new Task();
        }

    }

    private int getSpinnerPositionForUser(final long userId) {
        for (int i = 0; i < usersList.size(); i++) {
            if (userId == usersList.get(i).getId()) {
                return i;
            }
        }
        return 0;
    }

    private void initUsersSpinner(final UserDao userDao) {
        final ArrayList<String> usersName = new ArrayList<>();
        usersList = userDao.queryBuilder().list();

        for (Iterator<User> i = usersList.iterator(); i.hasNext(); ) {
            final String name = i.next().getName();
            usersName.add(name);
        }

        ArrayAdapter<String> usersAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,
                usersName);

        // Specify the layout to use when the list of choices appears
        usersAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        usersSpinner.setAdapter(usersAdapter);

        usersSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id) {
                final Long selectedUserId = usersList.get(position).getId();
                currentTask.setUserId(selectedUserId);
            }

            @Override
            public void onNothingSelected(final AdapterView<?> parent) {
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (saveOrEditTaskInDatabaseAsyncTask != null) {
            saveOrEditTaskInDatabaseAsyncTask.cancel(true);
        }
    }

    @OnClick(R.id.floating_action_button)
    public void onFabClicked() {
        if (editMode) {
            // during edit mode we display "done" aka "save" icon on fab button
            String title = titleEditText.getText().toString();
            String description = descriptionEditText.getText().toString();

            currentTask.setTitle(title);
            currentTask.setDescription(description);

            saveOrEditTaskInDatabaseAsyncTask = new SaveOrEditTaskInDatabaseAsyncTask();
            saveOrEditTaskInDatabaseAsyncTask.execute();
        } else {
            // user clicked on the pencil icon so he wants to enable edit mode
            setEditMode();
        }
    }

    private void setReadonlyMode() {
        editMode = false;
        titleEditText.setEnabled(false);
        descriptionEditText.setEnabled(false);
        usersSpinner.setEnabled(false);
        floatingActionButton.setImageResource(R.drawable.ic_mode_edit_white_24px);
    }

    private void setEditMode() {
        editMode = true;
        titleEditText.setEnabled(true);
        descriptionEditText.setEnabled(true);
        usersSpinner.setEnabled(true);
        floatingActionButton.setImageResource(R.drawable.ic_done_white_24px);
    }

    private class SaveOrEditTaskInDatabaseAsyncTask extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            if (currentTask.isTaskInDatabase()) {

                taskDao.update(currentTask);

                return true;
            } else {
                // we're saving new task in database
                taskDao.insert(currentTask);

                return true;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            super.onPostExecute(success);

            String message;
            if (success) {
                message = "Task saved!";
                setReadonlyMode();
            } else {
                message = "Error occurred..";
                setEditMode();
            }

            Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_LONG)
                    .show();
        }
    }
}
